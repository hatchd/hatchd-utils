# hatchd-utils

This is a small utility library that contains useful helper functions for usage in JS apps put together by [Hatchd][hatchd].

This project was bootstrapped with [TSDX](https://github.com/jaredpalmer/tsdx).

## Local Development

Below is a list of commands you will probably find useful.

### `npm start` or `yarn start`

Runs the project in development/watch mode. Your project will be rebuilt upon changes. TSDX has a special logger for you convenience. Error messages are pretty printed and formatted for compatibility VS Code's Problems tab.

Your library will be rebuilt if you make edits.

### `npm run build` or `yarn build`

Bundles the package to the `dist` folder.
The package is optimized and bundled with Rollup into multiple formats (CommonJS, UMD, and ES Module).

### `npm test` or `yarn test`

Runs the test watcher (Jest) in an interactive mode.
By default, runs tests related to files changed since the last commit.

## :warning: Updating node version

Current: `v12.17.0`

If you want to bump the node version that is used by this library, please change and align the version in the following places:

- `.nvmrc`
- `bitbucket-pipelines.yml` - the `image` field in each step.

[hatchd]: https://www.hatchd.com.au/
