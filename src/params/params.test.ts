import { getParams } from './params';

describe('getParams', () => {
  beforeAll(() => {
    window.history.pushState({}, 'Test Title', '/test.html?test=answer');
  });

  it('gets all search params from window location', () => {
    const params = getParams(window.location.search);
    expect(params).toStrictEqual({ test: 'answer' });
  });

  it('gets all search params from query string', () => {
    const params = getParams('test=answer&foo=bar%20baz');
    expect(params).toStrictEqual({ test: 'answer', foo: 'bar baz' });
  });

  it('gets all search params from a URL', () => {
    const url = new URL(
      'http://example.com/test.html?test=answer&foo=bar&baz=1,2,3&qux&1=1'
    );
    type Obj = '1' | 'test' | 'foo' | 'baz' | 'qux';
    const params = getParams<Obj>(url.search);

    expect(params).toStrictEqual<typeof params>({
      test: 'answer',
      foo: 'bar',
      baz: '1,2,3',
      qux: '',
      1: '1',
    });
  });

  it('returns empty object when no query strings are set', () => {
    const url = new URL('http://example.com/test.html');
    const params = getParams(url.search);
    expect(params).toStrictEqual({});
  });

  it('returns empty object if parameters are undefined', () => {
    const params = getParams();
    expect(params).toStrictEqual({});
  });
});
