import { createLogger } from './logger';
const logger = createLogger(true);

describe('logger', () => {
  it('can log formatted text', () => {
    console.log = jest.fn();
    logger.log('hello');
    expect(console.log).toHaveBeenCalledWith(
      '%c %s %c',
      'color: #fff; background-color: #333;',
      'LOG',
      '',
      'hello'
    );
  });
});
